//
//  UIColor+CustomColors.h
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (UIColor *)appBasicColor;

@end
