//
//  BaseModalPopupView.h
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BaseModalPopupView;

@protocol BaseModelViewDelegate <NSObject>
 @optional
  - (void)modalViewWillAppear:(id)view;
  - (void)modalViewDidAppear:(id)view;
  - (void)modalViewWillDisappear:(id)view;
  - (void)modalViewDidDisappear:(id)view;

@end

@interface BaseModalPopupView : UIView
@property (nonatomic, assign) id<BaseModelViewDelegate>delegate;

@property float animationAppear;
@property float animationDisappear;

@property BOOL isActive;

- (void)show;
- (void)hide;

- (CGSize)modalSize;

@end
