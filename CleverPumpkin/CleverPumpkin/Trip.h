//
//  Trip.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Flight.h"
#import "TakeOff.h"
#import "Landing.h"

@interface Trip : NSObject

@property float price;

@property (nonatomic, strong) NSDate *duration;
@property (nonatomic, strong) Flight *flight;
@property (nonatomic, strong) TakeOff *takeoff;
@property (nonatomic, strong) Landing *landing;

@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, copy) NSString *description;

@end
