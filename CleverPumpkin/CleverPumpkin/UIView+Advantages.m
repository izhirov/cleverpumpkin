//
//  UIView+Advantages.m
//  RecFlow
//
//  Created by Ivan Zhirov on 08.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import "UIView+Advantages.h"

@implementation UIView (Advantages)

- (void)roundingCorners:(UIRectCorner)corners
            cornerRadii:(CGSize)cornerRadii {
  UIBezierPath *maskPath;
  maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                   byRoundingCorners:corners
                                         cornerRadii:cornerRadii];
  
  CAShapeLayer *maskLayer = [CAShapeLayer new];
  maskLayer.frame = self.bounds;
  maskLayer.path = maskPath.CGPath;
  self.layer.mask = maskLayer;
}

@end
