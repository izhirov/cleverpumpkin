//
//  TrobberView.m
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import "TrobberView.h"

@implementation TrobberView
@synthesize title;


+ (TrobberView *)instance {
  static dispatch_once_t once;
  static TrobberView *singleton;
  dispatch_once(&once, ^ { singleton = [[TrobberView alloc] init]; });
  return singleton;
}

+ (void)setOn:(UIView *)view
    withTitle:(NSString *)text {
  
  TrobberView *this = [TrobberView instance];
  this.animationAppear = 0;
  this.animationDisappear = 0.5;
  this.title = text;
  
  UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, [this modalSize].height-40, [this modalSize].width, 30)];
  label.text = Localized(text);
  label.font = [UIFont fontWithName:@"Helvetica" size:15.0f];
  label.textColor = [UIColor grayColor];
  label.textAlignment = NSTextAlignmentCenter;
  
  [this addSubview:label];
  
  UIActivityIndicatorView *loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
  loader.frame = CGRectMake(([this modalSize].width-100)/2, 0, 100, 100);
  loader.color = [UIColor appBasicColor];
  
  [this addSubview:loader];
  
  // add to window
  [view addSubview:this];
  
  [loader startAnimating];
}

- (void)show {
  [super show];
  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)hide {
  [super hide];
  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}



- (CGSize)modalSize {
  return CGSizeMake(150, 120);
}


@end
