//
//  AirItemViewController.m
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import "AirItemViewController.h"
#import "CPConnection.h"

@implementation AirItemViewController

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  [[CPConnection instance] loadItemWithID:_flyID success:^(Trip *trip) {
    
    
    _photoImage.image = trip.photo;
    _companyName.text = trip.flight.carrier;
    _flyNumber.text = [NSString stringWithFormat:@"%d", trip.flight.number];
    _flyEG.text = [NSString stringWithFormat:@"%d", trip.flight.eq];
    
    _takeoffDate.text = parseDateToString(trip.takeoff.date);
    _takeoffTime.text = parseTimeToString(trip.takeoff.time);
    _takeoffCity.text = trip.takeoff.city;
    
    _landingDate.text = parseDateToString(trip.landing.date);
    _landingTime.text = parseTimeToString(trip.landing.time);
    _landingCity.text = trip.landing.city;
    
    _price.text = [NSString stringWithFormat:@"%.2f", trip.price];
    
    _description.text = trip.description;
    
  }];
  
}


@end
