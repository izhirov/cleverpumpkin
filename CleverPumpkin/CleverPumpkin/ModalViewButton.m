//
//  ModalViewButton.m
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import "ModalViewButton.h"

@implementation ModalViewButton {
  UILabel *label;
}
@synthesize title;

+ (ModalViewButton *)buttonWithTitle:(NSString *)button
                              target:(id)target
                         andSelector:(SEL)selector {
  
  ModalViewButton *this = [ModalViewButton new];
  this.title = button;

  [this addTarget:target
           action:selector
 forControlEvents:UIControlEventTouchUpInside];
  
  [this addTarget:this
           action:@selector(highlightDown)
 forControlEvents:UIControlEventTouchDown];
  
  [this addTarget:this
           action:@selector(highlightUp)
 forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
  
  this.backgroundColor = [UIColor clearColor];
  
  return this;
}

#pragma mark - Update buttons

- (void)update {
  UIView *topBorder = [[UIView alloc]
                       initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5)];
  topBorder.backgroundColor = [UIColor appBasicColor];
  [self addSubview:topBorder];
  
  label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
  label.text = self.title;
  label.font = [UIFont fontWithName:@"Helvetica"
                               size:15.0f];
  
  label.textColor = [UIColor appBasicColor];
  label.textAlignment = NSTextAlignmentCenter;
  label.backgroundColor = [UIColor clearColor];
  
  [self addSubview:label];
}


#pragma mark - Buttons visual highlight states control

- (void)highlightDown {
  self.backgroundColor = [UIColor appBasicColor];
  label.textColor = [UIColor whiteColor];
}

- (void)highlightUp {
  self.backgroundColor = [UIColor clearColor];
  label.textColor = [UIColor appBasicColor];
}

@end
