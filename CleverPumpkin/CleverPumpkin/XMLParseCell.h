//
//  XMLParseCell.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XMLParseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *price;

@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *flyNumber;
@property (weak, nonatomic) IBOutlet UILabel *flyEG;

@property (weak, nonatomic) IBOutlet UILabel *takeoffDate;
@property (weak, nonatomic) IBOutlet UILabel *takeoffTime;
@property (weak, nonatomic) IBOutlet UILabel *takeOffCity;

@property (weak, nonatomic) IBOutlet UILabel *landingDate;
@property (weak, nonatomic) IBOutlet UILabel *landingTime;
@property (weak, nonatomic) IBOutlet UILabel *landingCity;

@end
