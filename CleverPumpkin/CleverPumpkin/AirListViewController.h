//
//  AirListViewController.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirListViewController : UITableViewController
<NSXMLParserDelegate>
//@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegment;

- (IBAction)sortItems:(id)sender;

@end
