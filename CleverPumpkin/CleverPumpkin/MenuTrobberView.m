//
//  MenuTrobberView.m
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import "MenuTrobberView.h"
#import "ModalViewButton.h"

@interface MenuTrobberView()
 - (void)UISynchronize;

@end


@implementation MenuTrobberView {
  NSMutableArray *buttons;
  float dynamicHeight;
}


@synthesize title, delegate;


- (id)initWithTitle:(NSString *)text
         andButtons:(ModalViewButton *)button, ... {
  
  dynamicHeight = 50;
  
  if(self=[super init]) {
    buttons = [NSMutableArray new];
    
    id eachObject;
    va_list argumentList;
    if (button) {
      
      va_start(argumentList, button);
      [buttons addObject:button];
      
      while ((eachObject = va_arg(argumentList, id))) {
        [buttons addObject:eachObject];
      }
      
      va_end(argumentList);
    }
    
    self.animationAppear = 0.3;
    self.animationDisappear = 0.3;
    self.title = text;
  }
  return self;
}

- (void)UISynchronize {
  UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 250, 30)];
  label.text = self.title;
  label.font = [UIFont fontWithName:@"Helvetica" size:15.0f];
  label.textColor = [UIColor grayColor];
  label.textAlignment = NSTextAlignmentCenter;
  label.backgroundColor = [UIColor clearColor];
  
  [self addSubview:label];

  
  float dy = 0;
  for (ModalViewButton *button in buttons) {
    
    [self addSubview:button];
    button.frame = CGRectMake(0, 50+dy, 250, 40);
    [button update];
    
    dy+=button.frame.size.height;
    
    if ([button isEqual:[buttons lastObject]]) {
      [button roundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                  cornerRadii:CGSizeMake(5.0, 5.0)];
    }
    
  }
  
  dynamicHeight += dy;
}

- (CGSize)modalSize {
  [self UISynchronize];
  return CGSizeMake(250, dynamicHeight);
}

@end
