//
//  Helpers.m
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 12.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import "Helpers.h"

NSDate *parseTimeStringToDate(NSString *dateString) {
  NSDateFormatter *dateFormat = [NSDateFormatter new];
  [dateFormat setDateFormat:@"HH:mm"];
  NSDate *date = [dateFormat dateFromString:dateString];
  return date;
}

NSDate *parseDateStringToDate(NSString *dateString) {
  NSDateFormatter *dateFormat = [NSDateFormatter new];
  [dateFormat setDateFormat:@"YYYY-MM-DD"];
  NSDate *date = [dateFormat dateFromString:dateString];
  return date;
}

NSString *parseTimeToString(NSDate *date) {
  NSDateFormatter *dateFormat = [NSDateFormatter new];
  [dateFormat setDateFormat:@"HH:mm"];
  return [dateFormat stringFromDate:date];
}

NSString *parseDateToString(NSDate *date) {
  NSDateFormatter *dateFormat = [NSDateFormatter new];
  [dateFormat setDateFormat:@"YYYY.MM.DD"];
  return [dateFormat stringFromDate:date];
}