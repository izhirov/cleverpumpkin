//
//  BaseModalPopupView.m
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import "BaseModalPopupView.h"

@implementation BaseModalPopupView
@synthesize delegate;
@synthesize animationAppear, animationDisappear;
@synthesize isActive;

- (id)init {
  if (self=[super init]) {
    self.animationAppear = 1.0;
    self.animationDisappear = 1.0f;
  
    self.layer.masksToBounds = NO;
    
    self.layer.cornerRadius = 5.0f;
    self.layer.borderColor = [UIColor appBasicColor].CGColor; // debug
    self.layer.borderWidth = 0.8f;
    
    self.layer.shadowColor = [UIColor appBasicColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 1.2f);
    self.layer.shadowRadius = 0.5f;
    self.layer.shadowOpacity = 0.5f;
    
    self.backgroundColor = [UIColor whiteColor];
  }
  return self;
}

- (void)show {
  
  if (isActive) {
    [self hide];
    return;
  }
  
  isActive = YES;
  
  UIWindow *window = [UIApplication sharedApplication].windows[0];
  CGSize size = [self modalSize];
  CGRect rect = CGRectMake(0, 0, size.width, size.height);

  [window bringSubviewToFront:self];
  
  self.frame = rect;
  self.center = CGPointMake(-self.frame.size.width, window.center.y);
  
  // if we wont animations
  if (self.animationAppear == 0) {
    self.center = window.center;
    
    if (delegate && [delegate respondsToSelector:@selector(modalViewDidAppear:)]) {
      [delegate modalViewDidAppear:self];
    }
    
    return;
  }
  
  
  if (delegate && [delegate respondsToSelector:@selector(modalViewWillAppear:)]) {
    [delegate modalViewWillAppear:self];
  }
  
  [UIView animateWithDuration:self.animationAppear animations:^{
    
    self.center = window.center;

  } completion:^(BOOL finished) {
    if (finished) {
      
      if (delegate && [delegate respondsToSelector:@selector(modalViewDidAppear:)]) {
        [delegate modalViewDidAppear:self];
      }
      
    }
  }];
  
}


- (void)hide {
  UIWindow *window = [UIApplication sharedApplication].windows[0];
  isActive = NO;
  
  if (delegate && [delegate respondsToSelector:@selector(modalViewWillDisappear:)]) {
    [delegate modalViewWillDisappear:self];
  }
  
  // if we wont animations
  if (self.animationDisappear == 0) {
    
    self.center = CGPointMake(-self.frame.size.width, window.center.y);
    
    if (delegate && [delegate respondsToSelector:@selector(modalViewDidDisappear:)]) {
      [delegate modalViewDidDisappear:self];
    }
    
    return;
  }
  
  
  [UIView animateWithDuration:self.animationDisappear animations:^{
    
    self.center = CGPointMake(-self.frame.size.width, window.center.y);
    
  } completion:^(BOOL finished) {
    if (finished) {
      
      if (delegate && [delegate respondsToSelector:@selector(modalViewDidDisappear:)]) {
        [delegate modalViewDidDisappear:self];
      }
      
      //[self removeFromSuperview];
    }
  }];
}


- (CGSize)modalSize {
  NSLog(@"Should implement in not abstract class like this");
  return CGSizeZero;
}


@end
