//
//  MenuTrobberView.h
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseModalPopupView.h"
#import "ModalViewButton.h"


@protocol MenuTrobberViewDelegate <BaseModelViewDelegate>
  @optional
    - (void)menuItemDidTouched:(ModalViewButton *)button;

@end

@interface MenuTrobberView : BaseModalPopupView
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) id<MenuTrobberViewDelegate>delegate;

- (id)initWithTitle:(NSString *)title
         andButtons:(ModalViewButton *)button, ... NS_REQUIRES_NIL_TERMINATION;


@end
