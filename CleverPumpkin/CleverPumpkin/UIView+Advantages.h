//
//  UIView+Advantages.h
//  RecFlow
//
//  Created by Ivan Zhirov on 08.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Advantages)

- (void)roundingCorners:(UIRectCorner)corners
            cornerRadii:(CGSize)cornerRadii;


@end
