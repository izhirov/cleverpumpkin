//
//  AirItemViewController.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirItemViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *flyNumber;
@property (weak, nonatomic) IBOutlet UILabel *flyEG;

@property (weak, nonatomic) IBOutlet UILabel *takeoffDate;
@property (weak, nonatomic) IBOutlet UILabel *takeoffTime;
@property (weak, nonatomic) IBOutlet UILabel *takeoffCity;

@property (weak, nonatomic) IBOutlet UILabel *landingDate;
@property (weak, nonatomic) IBOutlet UILabel *landingTime;
@property (weak, nonatomic) IBOutlet UILabel *landingCity;

@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *description;

@property int flyID; // need to load new xml

@end
