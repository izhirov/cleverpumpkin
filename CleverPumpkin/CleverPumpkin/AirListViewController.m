//
//  AirListViewController.m
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import "AirListViewController.h"
#import "XMLParseCell.h"

#import "CPConnection.h"
#import "Trip.h"

#import "AirItemViewController.h"

@implementation AirListViewController {
  NSMutableArray *airData;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
      airData = [@[] mutableCopy];
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];

  // Load items in tableview
  [[CPConnection instance] loadData:^(NSMutableArray *data) {
    airData = data;
    [self.tableView reloadData];
  }];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [airData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"AirCell";
    XMLParseCell *cell = (XMLParseCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                                         forIndexPath:indexPath];
  
    Trip *trip = airData[indexPath.row];
  
    cell.price.text = [NSString stringWithFormat:@"%.2f", trip.price];
    cell.companyName.text = trip.flight.carrier;
    cell.flyNumber.text = [NSString stringWithFormat:@"%d", trip.flight.number];
    cell.flyEG.text = [NSString stringWithFormat:@"%d", trip.flight.eq];
  
    cell.duration.text = parseTimeToString(trip.duration);
    cell.takeoffDate.text = parseDateToString(trip.takeoff.date);
    cell.takeoffTime.text = parseTimeToString(trip.takeoff.time);
    cell.takeOffCity.text = trip.takeoff.city;

    cell.landingDate.text = parseDateToString(trip.landing.date);
    cell.landingTime.text = parseTimeToString(trip.landing.time);
    cell.landingCity.text = trip.landing.city;
  
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([[segue identifier] isEqualToString:@"ShowAirDetail"])
  {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Trip *trip = airData[indexPath.row];
    AirItemViewController *detailVC = [segue destinationViewController];
    detailVC.flyID = trip.flight.number;
  }
}

- (IBAction)sortItems:(id)sender {
  UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Sort by"
                                                  delegate:nil
                                         cancelButtonTitle:@"Cancel"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Price", @"Duration", nil];
  
  as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
  as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex){

    // cancel button clicked
    if (buttonIndex == actionSheet.cancelButtonIndex) {
      return;
    }
    
    NSString *keyToSort;
    if (buttonIndex == 0) {
      keyToSort = @"price";
    }
    if (buttonIndex == 1) {
      keyToSort = @"duration";
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:keyToSort ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *newData = [airData sortedArrayUsingDescriptors:sortDescriptors];
    
    airData = (NSMutableArray *)newData; // sorted array
    
    [self.tableView reloadData];
  };
  
  [as showInView:self.view];
}

@end
