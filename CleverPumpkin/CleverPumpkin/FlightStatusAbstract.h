//
//  FlightStatusAbstract.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlightStatusAbstract : NSObject

@property (nonatomic, strong) NSDate *date, *time;
@property (nonatomic, copy) NSString *city;

@end
