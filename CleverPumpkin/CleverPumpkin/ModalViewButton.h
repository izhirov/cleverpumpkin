//
//  ModalViewButton.h
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewButton : UIButton

@property (nonatomic, copy) NSString *title;

+ (ModalViewButton *)buttonWithTitle:(NSString *)button
                              target:(id)target
                         andSelector:(SEL)selector;

- (void)update;

@end
