//
//  TrobberView.h
//  RecFlow
//
//  Created by Ivan Zhirov on 07.01.14.
//  Copyright (c) 2014 ddgcorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseModalPopupView.h"

@interface TrobberView : BaseModalPopupView
@property (nonatomic, copy) NSString *title;

+ (TrobberView *)instance;
+ (void)setOn:(UIView *)view
    withTitle:(NSString *)title;


@end
