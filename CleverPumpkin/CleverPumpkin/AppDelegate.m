//
//  AppDelegate.m
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import "AppDelegate.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
    [TrobberView setOn:_window
            withTitle:@"Loading..."];
  
    return YES;
}

@end
