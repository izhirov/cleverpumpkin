//
//  Helpers.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 12.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

NSDate *parseDateStringToDate(NSString *dateString);
NSDate *parseTimeStringToDate(NSString *dateString);

NSString *parseTimeToString(NSDate *date);
NSString *parseDateToString(NSDate *date);
