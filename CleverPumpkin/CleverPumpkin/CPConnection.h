//
//  CPConnection.h
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML+HTTP.h"
#import "Trip.h"
#import "TrobberView.h"

@interface CPConnection : NSObject

+ (CPConnection *)instance;

- (void)loadData:(void(^)(NSMutableArray *data))success;
- (void)loadItemWithID:(int)flyID
               success:(void(^)(Trip *trip))success;

@end
