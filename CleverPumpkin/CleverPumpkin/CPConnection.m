//
//  CPConnection.m
//  CleverPumpkin
//
//  Created by Ivan Zhirov on 11.03.14.
//  Copyright (c) 2014 Ivan Zhirov. All rights reserved.
//

#import "CPConnection.h"
#import "TBXML.h"

@interface CPConnection()
- (Trip *)parseTrip:(TBXMLElement *)element;

@end

@implementation CPConnection

+ (CPConnection *)instance {
  static dispatch_once_t once;
  static CPConnection *instance;
  dispatch_once(&once, ^ { instance = [CPConnection new]; });
  return instance;
}

- (void)loadData:(void(^)(NSMutableArray *data))success {
  
  [[TrobberView instance] show]; // loader
  
  [TBXML newTBXMLWithURL:[NSURL URLWithString:XML_PAGE_URL] success:^(TBXML *tbxmlDocument) {
    NSMutableArray *data = [NSMutableArray array];
    
    TBXMLElement *rootXMLElement = tbxmlDocument.rootXMLElement;
    if (rootXMLElement) {
      TBXMLElement *trip = [TBXML childElementNamed:@"trip"
                                      parentElement:rootXMLElement];
      
      while (trip != nil) {
        [data addObject:[self parseTrip:trip]];
        trip = [TBXML nextSiblingNamed:@"trip" searchFromElement:trip];
      }
      
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
      success(data);
      [[TrobberView instance] hide];
    });
    
  } failure:^(TBXML *tbxml, NSError *error) {
    NSLog(@"tbxml error %@", [error localizedFailureReason]);
    
  }];
}

- (void)loadItemWithID:(int)flyID
               success:(void(^)(Trip *trip))success {
  
  [[TrobberView instance] show]; //loader
  
  [TBXML newTBXMLWithURL:[NSURL URLWithString:XML_FLIGHT_URL(flyID)] success:^(TBXML *tbxmlDocument) {
    
    TBXMLElement *rootXMLElement = tbxmlDocument.rootXMLElement;
    if (rootXMLElement) {
      
      Trip *trip = [self parseTrip:[TBXML childElementNamed:@"trip"
                                              parentElement:rootXMLElement]];
      
      dispatch_async(dispatch_get_main_queue(), ^{
        success(trip);
        [[TrobberView instance] hide];
      });
    }
    
  } failure:^(TBXML *tbxml, NSError *error) {
    NSLog(@"tbxml error %@", [error localizedFailureReason]);
  }];

}

- (Trip *)parseTrip:(TBXMLElement *)trip {
  
  Trip *_tripModel = [Trip new];
  
  
  // duration
  _tripModel.duration = parseTimeStringToDate([TBXML valueOfAttributeNamed:@"duration"
                                                                forElement:trip]);
  
  // for item object parse photo and description only
  //
  // description
  TBXMLElement *description = [TBXML childElementNamed:@"description"
                                   parentElement:trip];
  if (description) {
    _tripModel.description = [TBXML textForElement:description];
  }
  
  // photo
  TBXMLElement *photo = [TBXML childElementNamed:@"photo"
                                   parentElement:trip];
  if (photo) {
    _tripModel.photo = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                               [NSURL URLWithString:[TBXML valueOfAttributeNamed:@"src"
                                                                                      forElement:photo]]]];
  }
  
  // price
  TBXMLElement *price = [TBXML childElementNamed:@"price"
                                   parentElement:trip];
  if (price)
    _tripModel.price = [[TBXML textForElement:price] floatValue];
  
  
  // takeoff
  TakeOff *takeoff = [TakeOff new];
  TBXMLElement *takeoffXml = [TBXML childElementNamed:@"takeoff"
                                        parentElement:trip];
  
  takeoff.date = parseDateStringToDate([TBXML valueOfAttributeNamed:@"date"
                                                         forElement:takeoffXml]);
  
  takeoff.time = parseTimeStringToDate([TBXML valueOfAttributeNamed:@"time"
                                                         forElement:takeoffXml]);
  
  takeoff.city = [TBXML valueOfAttributeNamed:@"city"
                                   forElement:takeoffXml];
  _tripModel.takeoff = takeoff;
  
  // landing
  Landing *landing = [Landing new];
  TBXMLElement *landingXml = [TBXML childElementNamed:@"landing"
                                        parentElement:trip];
  
  landing.date = parseDateStringToDate([TBXML valueOfAttributeNamed:@"date"
                                                         forElement:landingXml]);
  
  landing.time = parseTimeStringToDate([TBXML valueOfAttributeNamed:@"time"
                                                         forElement:landingXml]);
  
  landing.city = [TBXML valueOfAttributeNamed:@"city"
                                   forElement:landingXml];
  
  _tripModel.landing = landing;
  
  // flight
  Flight *flight = [Flight new];
  TBXMLElement *flightXml = [TBXML childElementNamed:@"flight"
                                       parentElement:trip];
  
  flight.carrier = [TBXML valueOfAttributeNamed:@"carrier"
                                     forElement:flightXml];
  
  flight.number = [[TBXML valueOfAttributeNamed:@"number"
                                     forElement:flightXml] intValue];
  
  flight.eq = [[TBXML valueOfAttributeNamed:@"eq"
                                 forElement:flightXml] intValue];
  
  _tripModel.flight = flight;
  
  return _tripModel;
}

@end
